import UIKit

// standart types
var str = "Hello, playground"
str = "Hello, Swift"
let constStr = str

// speicfiing types
var nextYear: Int = 0
var bodyTemp: Float = 0
var hasPet: Bool = true

// collection types
var ints: Array<Int> = []
var strings: [String] = []
var dictionaryOfCapitalsByCountry: [String: String] = [:]
var winningLotteryNumbers: Set<Int> = []

// literals and subscipting
let number = 42
let fmStation = 91.2

let countingUp = ["one", "two"]
let secondElement = countingUp[1]
let nameByParkingSpace = [11: "Alice", 41: "Bob"]

// initializers
let emptyString = String()
let emptyArrayOfInts = [Int]()
let emptySet = Set<Float>()

let defaultNumber = Int()
let defaultBool = Bool()

let meaningOfLive = String(number)
let availableRooms = Set([102, 344, 102, 332])

let defaultFloat = Float()
let floatFromLiteral = Float(3.14)
let easyPi = 3.14
let floatFromDouble = Float(easyPi)
let floatingPi: Float = 3.14

// properties
countingUp.count
emptyString.isEmpty

// instance methods
var secondCountingUp = ["one", "two"]
secondCountingUp.append("three")

// optionals
var reading1: Float?
var reading2: Float?
var reading3: Float?

reading1 = 4.2
reading2 = 4.0
reading3 = 5.1

if let r1 = reading1,
   let r2 = reading2,
   let r3 = reading3 {
    let averageReading = (r1 + r2 + r3) / 3
    print(averageReading)
} else {
    let errorString = "Instrument reported a reading that was nil."
    print(errorString)
}

// subscriping dictionaries
// let space11Assignee: String? = nameByParkingSpace[11]
let space42Assignee: String? = nameByParkingSpace[42]

if let space11Assignee = nameByParkingSpace[11] {
    print(space11Assignee)
}

// loops and string interpolation
for (space, name) in nameByParkingSpace {
    let permit = "Space \(space): \(name)"
    print(permit)
}

// enumerations and switch
enum PieType: Int {
    case apple = 0
    case cherry
    case pecan
}

let favoritePie = PieType.apple

let name: String
switch favoritePie {
case .apple:
    name = "Apple"
case .cherry:
    name = "Cherry"
case .pecan:
    name = "Pecan"
}

// enumerations and raw values
let pieRawValue = PieType.pecan.rawValue
if let pieType = PieType(rawValue: pieRawValue) {
    // valid pieType
    print(pieType)
}

// closures
let compareAscending = { (i: Int, j: Int) -> Bool in
    return i < j
}
compareAscending(-1, 21)
compareAscending(21, 12)

var numbers = [42, 9, 12, -17]
numbers.sort(by: compareAscending)
numbers.sort(by: {(i, j) -> Bool in return i > j})
numbers


