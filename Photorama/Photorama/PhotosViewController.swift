//
//  ViewController.swift
//  Photorama
//
//  Created by Michal Němec on 27.02.2021.
//

import UIKit

class PhotosViewController: UIViewController, UICollectionViewDelegate {

    @IBOutlet private var switchButton: UIBarButtonItem!
    @IBOutlet private var collectionView: UICollectionView!
    
    private var fetchingURL = FlickAPI.InterestingPhotosURL
    private var interesting = true
    var store: PhotoStore!
    let photoDataSource = PhotoDataSource()
    
    @IBAction func switchButtonTapped(_ sender: UIBarButtonItem) {
        var newTitle: String!
        if interesting {
            print("recents")
            newTitle = "Recents"
            fetchingURL = FlickAPI.RecentPhotosURL
        } else {
            print("interesting")
            newTitle = "Interesting"
            fetchingURL = FlickAPI.InterestingPhotosURL
        }
        switchButton.title = newTitle
        
        interesting = !interesting
        updatePhotos()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.dataSource = photoDataSource
        collectionView.delegate = self
        
        updateDataSource()
        
//        switchButton.title = "Interesting"
        switchButton.title = ""
        switchButton.isEnabled = false
        updatePhotos()
    }
    
    private func updatePhotos() {
        store.fetchPhotos(url: fetchingURL) {
            (photosResult) in
            self.updateDataSource()
        }
    }
    
    private func updateDataSource() {
        store.fetchAllPhotos { (photosResult) in
            switch photosResult {
            case let .success(photos):
                self.photoDataSource.photos.removeAll()
                self.photoDataSource.photos = photos
                print("new photo count: \(self.photoDataSource.photos.count)")
            case .failure:
                self.photoDataSource.photos.removeAll()
            }
            self.collectionView.reloadSections(IndexSet(integer: 0))
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let photo = photoDataSource.photos[indexPath.row]
        
        // Download the image data, which cloud takke some time
        store.fetchImage(for: photo) {(result) -> Void in
            
            // The index path for the photo might have changed vetween the
            // time the rquest started and finished, so find the most
            // recent index path
            guard let photoIndex = self.photoDataSource.photos.firstIndex(of: photo), case let .success(image) = result else {
                return
            }
            let photoIndexPath = IndexPath(item: photoIndex, section: 0)
            
            // When the request finishes, find the current cell for this photo
            OperationQueue.main.addOperation {
                if let cell = self.collectionView.cellForItem(at: photoIndexPath) as? PhotoCollectionViewCell {
                    cell.update(displaying: image)
                }
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "showPhoto":
            if let selectedIndexPath = collectionView.indexPathsForSelectedItems?.first {
                let photo = photoDataSource.photos[selectedIndexPath.row]
                
                photo.viewCount += 1
                
                let destinationVC = segue.destination as! PhotoInfoViewController
                destinationVC.photo = photo
                destinationVC.store = store
            }
        default:
            preconditionFailure("Unexpected segue identifier.")
        }
    }
}
