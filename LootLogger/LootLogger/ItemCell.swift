//
//  ItemCell.swift
//  LootLogger
//
//  Created by Michal Němec on 24.02.2021.
//

import UIKit

class ItemCell: UITableViewCell {
    
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var serialNumberLabe: UILabel!
    @IBOutlet var valueLabel: UILabel!
    
}
