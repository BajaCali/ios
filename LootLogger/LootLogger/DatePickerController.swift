//
//  DatePickerController.swift
//  LootLogger
//
//  Created by Michal Němec on 24.02.2021.
//

import UIKit

class DatePickerController: UIViewController {
    
    var item: Item!
    
    @IBAction func dateChanged(datePicker: UIDatePicker) {
        item.dateCreated = datePicker.date
    }
    
}
