//
//  MoodsConfigurable.swift
//  Mandala
//
//  Created by Michal Němec on 26.02.2021.
//

import UIKit

protocol MoodsConfigurable {
    func add(_ moodEntry: MoodEntry)
}
