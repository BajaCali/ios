//
//  MoodEntry.swift
//  Mandala
//
//  Created by Michal Němec on 26.02.2021.
//

import UIKit

struct MoodEntry {
    var mood: Mood
    var timestamp: Date
}
