//
//  ImageSelector.swift
//  Mandala
//
//  Created by Michal Němec on 26.02.2021.
//

import UIKit

class ImageSelector: UIControl {
    
    var selectedIndex = 0 {
        didSet {
            if selectedIndex < 0 {
                selectedIndex = 0
            }
            if selectedIndex >= imageButtons.count {
                selectedIndex = imageButtons.count - 1
            }
            
            highlightView.backgroundColor = highlightColor(forIndex: selectedIndex)
            
            let imageButton = imageButtons[selectedIndex]
            highlightViewXConstraint = highlightView.centerXAnchor.constraint(equalTo: imageButton.centerXAnchor)
        }
    }
    
    private let selectorStackView: UIStackView = {
        let stackVeiw = UIStackView()
        
        stackVeiw.axis = .horizontal
        stackVeiw.distribution = .fillEqually
        stackVeiw.alignment = .center
        stackVeiw.spacing = 12.0
        stackVeiw.translatesAutoresizingMaskIntoConstraints = false
        
        return stackVeiw
    }()
    private let highlightView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private var highlightViewXConstraint: NSLayoutConstraint! {
        didSet {
            oldValue?.isActive = false
            highlightViewXConstraint.isActive = true
        }
    }
    
    private var imageButtons: [UIButton] = [] {
        didSet {
            oldValue.forEach { $0.removeFromSuperview() }
            imageButtons.forEach { selectorStackView.addArrangedSubview($0) }
        }
    }
    var images: [UIImage] = [] {
        didSet {
            imageButtons = images.map { image in
                let button = UIButton()
                button.setImage(image, for: .normal)
                button.imageView?.contentMode = .scaleAspectFit
                button.adjustsImageWhenHighlighted = false
                button.addTarget(self, action: #selector(imageButtonTapped(_:)), for: .touchUpInside)
                return button
            }
            selectedIndex = 0
        }
    }
    var highlightColors: [UIColor] = [] {
        didSet {
            highlightView.backgroundColor = highlightColor(forIndex: selectedIndex)
        }
    }
    
    private func highlightColor(forIndex index: Int) -> UIColor {
        guard index >= 0 && index < highlightColors.count else {
            return UIColor.blue.withAlphaComponent(0.6)
        }
        return highlightColors[index]
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        highlightView.layer.cornerRadius = highlightView.bounds.height / 2.0
    }
    
    // MARK: - Initializers
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureViewHierarchy()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configureViewHierarchy()
    }
    
    // MARK: - Actions
    private func configureViewHierarchy() {
        addSubview(selectorStackView)
        insertSubview(highlightView, belowSubview: selectorStackView)
        
        NSLayoutConstraint.activate([
            selectorStackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            selectorStackView.trailingAnchor.constraint(equalTo: trailingAnchor),
            selectorStackView.bottomAnchor.constraint(equalTo: bottomAnchor),
            selectorStackView.topAnchor.constraint(equalTo: topAnchor),
            highlightView.heightAnchor.constraint(equalTo: highlightView.widthAnchor),
            highlightView.heightAnchor.constraint(equalTo: selectorStackView.heightAnchor, multiplier: 0.9),
            highlightView.centerYAnchor.constraint(equalTo: selectorStackView.centerYAnchor)
        ])
    }
    
    @objc private func imageButtonTapped(_ sender: UIButton) {
        guard let buttonIndex = imageButtons.firstIndex(of: sender) else {
            preconditionFailure("Unable to find the tapped button in the buttons array.")
        }
        
        let selectionAnimator = UIViewPropertyAnimator (duration: 0.6, dampingRatio: 0.6, animations: {
                self.selectedIndex = buttonIndex
                self.layoutIfNeeded()
            }
        )
        selectionAnimator.startAnimation()
        
        sendActions(for: .valueChanged)
    }
}
