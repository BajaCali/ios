//
//  ViewController.swift
//  WorldTrotter
//
//  Created by Michal Němec on 17.02.2021.
//

import UIKit

class ConversionViewController: UIViewController, UITextFieldDelegate {
    
    var degreesFahrenheit: UITextField!
    var degreesCelsius: UILabel!
    
    
    let numberFormatter: NumberFormatter = {
        let nf = NumberFormatter()
        nf.numberStyle = .decimal
        nf.minimumFractionDigits = 0
        nf.maximumFractionDigits = 1
        return nf
    }()

    var fahrenheitValue: Measurement<UnitTemperature>? {
            didSet {
                updateCelsiusLabel()
            }
        }
        var celsiusValue: Measurement<UnitTemperature>? {
            if let fahrenheitValue = fahrenheitValue {
                return fahrenheitValue.converted(to: .celsius)
            }
            return nil
        }
    
    
    override func loadView() {
        view = UIView()
        
        let tapRecognizer = UITapGestureRecognizer()
        tapRecognizer.isEnabled = true
        tapRecognizer.addTarget(self, action: #selector(dismissKeyboard(_:)))
        view.addGestureRecognizer(tapRecognizer)

        degreesFahrenheit = UITextField()
        degreesFahrenheit.delegate = self
        degreesFahrenheit.keyboardType = .decimalPad
        degreesFahrenheit.addTarget(self, action: #selector(fahrenheitFieldEditingChanged(_:)), for: .editingChanged)

        let degreesFahrenheitLabel = UILabel()
        let isReally = UILabel()
        degreesCelsius = UILabel()
        let degreesCelsiusLabel = UILabel()
        let s1 = UIView()
        let s2 = UIView()
        let s3 = UIView()
        let s4 = UIView()

        degreesFahrenheit.translatesAutoresizingMaskIntoConstraints = false
        degreesFahrenheitLabel.translatesAutoresizingMaskIntoConstraints = false
        isReally.translatesAutoresizingMaskIntoConstraints = false
        degreesCelsius.translatesAutoresizingMaskIntoConstraints = false
        degreesCelsiusLabel.translatesAutoresizingMaskIntoConstraints = false
        s1.translatesAutoresizingMaskIntoConstraints = false
        s2.translatesAutoresizingMaskIntoConstraints = false
        s3.translatesAutoresizingMaskIntoConstraints = false
        s4.translatesAutoresizingMaskIntoConstraints = false

        degreesFahrenheit.text = "value"
        degreesFahrenheitLabel.text = "degrees Fahrenheit"
        isReally.text = "is really"
        degreesCelsius.text = "100"
        degreesCelsiusLabel.text = "degrees Celsius"

        let orangeColor = UIColor.init(red: 225/255, green: 88/255, blue: 41/255, alpha: 1)
        let bigText = degreesCelsius.font.withSize(70)
        let complementaryText = degreesFahrenheitLabel.font.withSize(36)
        degreesFahrenheit.textColor = orangeColor
        degreesFahrenheitLabel.textColor = orangeColor
        degreesFahrenheit.font = bigText
        degreesCelsius.font = bigText
        degreesFahrenheitLabel.font = complementaryText
        isReally.font = complementaryText
        degreesCelsiusLabel.font = complementaryText

        view.addSubview(degreesFahrenheit)
        view.addSubview(s1)
        view.addSubview(degreesFahrenheitLabel)
        view.addSubview(s2)
        view.addSubview(isReally)
        view.addSubview(s3)
        view.addSubview(degreesCelsius)
        view.addSubview(s4)
        view.addSubview(degreesCelsiusLabel)

        // add horizontal constraints - center all
        let degreesFahrenheitXConstraint = degreesFahrenheit.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        let s1XConstraint = s1.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        let degreesFahrenheitLabelXConstraint = degreesFahrenheitLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        let s2XConstraint = s2.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        let isReallyXConstraint = isReally.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        let s3XConstraint = s3.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        let degreesCelsiusXConstraint = degreesCelsius.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        let s4XConstraint = s4.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        let degreesCelsiusLabelXConstraint = degreesCelsiusLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        degreesFahrenheitXConstraint.isActive = true
        s1XConstraint.isActive = true
        degreesFahrenheitLabelXConstraint.isActive = true
        s2XConstraint.isActive = true
        isReallyXConstraint.isActive = true
        s3XConstraint.isActive = true
        degreesCelsiusXConstraint.isActive = true
        s4XConstraint.isActive = true
        degreesCelsiusLabelXConstraint.isActive = true

        // add vertical constraints
        let degreesFahrenheitTopConstraint = degreesFahrenheit.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 8)
        let s1TopConstraint = s1.topAnchor.constraint(equalTo: degreesFahrenheit.bottomAnchor)
        let degreesFahrenheitLabelTopConstraint = degreesFahrenheitLabel.topAnchor.constraint(equalTo: s1.bottomAnchor)
        let s2TopConstraint = s2.topAnchor.constraint(equalTo: degreesFahrenheitLabel.bottomAnchor)
        let isReallyTopConstraint = isReally.topAnchor.constraint(equalTo: s2.bottomAnchor)
        let s3TopConstraint = s3.topAnchor.constraint(equalTo: isReally.bottomAnchor)
        let degreesCelsiusTopConstraint = degreesCelsius.topAnchor.constraint(equalTo: s3.bottomAnchor)
        let s4TopConstraint = s4.topAnchor.constraint(equalTo: degreesCelsius.bottomAnchor)
        let degreesCelsiusLabelTopConstraint = degreesCelsiusLabel.topAnchor.constraint(equalTo: s4.bottomAnchor)
        let degreesCelsiusLabelBottomConstraint = degreesCelsiusLabel.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -80)
        degreesFahrenheitTopConstraint.isActive = true
        s1TopConstraint.isActive = true
        degreesFahrenheitLabelTopConstraint.isActive = true
        s2TopConstraint.isActive = true
        isReallyTopConstraint.isActive = true
        s3TopConstraint.isActive = true
        degreesCelsiusTopConstraint.isActive = true
        s4TopConstraint.isActive = true
        degreesCelsiusLabelTopConstraint.isActive = true
        degreesCelsiusLabelBottomConstraint.isActive = true
        
        // create even spacing
        let s2HeightConstraint = s2.heightAnchor.constraint(equalTo: s1.heightAnchor)
        let s3HeightConstraint = s3.heightAnchor.constraint(equalTo: s1.heightAnchor)
        let s4HeightConstraint = s4.heightAnchor.constraint(equalTo: s1.heightAnchor)
        s2HeightConstraint.isActive = true
        s3HeightConstraint.isActive = true
        s4HeightConstraint.isActive = true
        
        // this blank layer that will be replaced by new gradient layer in viewWillAppear
        view.layer.insertSublayer(CALayer(), at: 0)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
     
        // create new gradient layer
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = view.bounds
        gradientLayer.colors = { () -> [CGColor] in
            var colors: [CGColor] = []
            for _ in (0..<Int.random(in: 2..<6)) {
                colors.append(UIColor.init(hue: CGFloat.random(in: 0..<1), saturation: CGFloat.random(in: 0..<1), brightness: CGFloat.random(in: 0..<1), alpha: 1).cgColor)
            }
            return colors
        }()
        
        if let sublayers = view.layer.sublayers {
            // replace last existing layer or the one created in viewDidLoad
            view.layer.replaceSublayer(sublayers[0], with: gradientLayer)
        }
    }
    
    
    @objc func dismissKeyboard(_ sender: UITapGestureRecognizer) {
        degreesFahrenheit.resignFirstResponder()
    }
    
    @objc func fahrenheitFieldEditingChanged(_ textField: UITextField) {
        if let text = textField.text, let value = Double(text) {
            fahrenheitValue = Measurement(value: value, unit: .fahrenheit)
        } else {
            fahrenheitValue = nil
        }
    }

    func updateCelsiusLabel() {
        if let celsiusValue = celsiusValue {
            degreesCelsius.text = numberFormatter.string(from: NSNumber(value: celsiusValue.value))
        } else {
            degreesCelsius.text = "???"
        }
    }
    

    func textFieldDidBeginEditing(_ textField: UITextField) {
        if let text = textField.text {
            if text.rangeOfCharacter(from: CharacterSet.decimalDigits) == nil {
                textField.text = ""
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let existingTextHasDecimalSeparator = textField.text?.range(of: ".")
        let replacementTextHasDecimalSeparator = string.range(of: ".")

        if string.rangeOfCharacter(from: CharacterSet.decimalDigits) == nil,
           string != ".",
           string != "" {
            return false
        }
        if existingTextHasDecimalSeparator != nil,
           replacementTextHasDecimalSeparator != nil {
            return false
        }
        return true
    }
}

