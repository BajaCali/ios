//
//  MapViewController.swift
//  WorldTrotter
//
//  Created by Michal Němec on 18.02.2021.
//

import UIKit
import MapKit

class MapViewController: UIViewController, MKMapViewDelegate {
    
    var mapView: MKMapView!
    var locationManager: CLLocationManager!
    
    override func loadView() {
        mapView = MKMapView()
        view = mapView
        mapView.delegate = self
        
        locationManager = CLLocationManager()
        locationManager.requestWhenInUseAuthorization()

        // map type selector
        let segmentedControl = UISegmentedControl(items: ["Standard", "Hybrid", "Satellite"])
        segmentedControl.backgroundColor = UIColor.systemBackground
        segmentedControl.selectedSegmentIndex = 0
        
        segmentedControl.addTarget(self, action: #selector(mapTypeChanged(_:)), for: .valueChanged)
        
        segmentedControl.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(segmentedControl)
        
        let margins = view.layoutMarginsGuide
        let segmentControlTopConstraint = segmentedControl.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 8)
        let segmentControlLeadingConstraint = segmentedControl.leadingAnchor.constraint(equalTo: margins.leadingAnchor)
        let segmentControlTrailingConstraint = segmentedControl.trailingAnchor.constraint(equalTo: margins.trailingAnchor)
        
        segmentControlTopConstraint.isActive = true
        segmentControlLeadingConstraint.isActive = true
        segmentControlTrailingConstraint.isActive = true
        
        // poi toggle
        let pointsOfInterestSwitch = UISwitch()
        pointsOfInterestSwitch.isOn = true
        pointsOfInterestSwitch.translatesAutoresizingMaskIntoConstraints = false
        
        pointsOfInterestSwitch.addTarget(self, action: #selector(pointsOfInterestSwitchChanged(_:)), for: . valueChanged)
        
        let pointsOfInterestLabel = UILabel(frame: CGRect())
        pointsOfInterestLabel.text = "Points of Interest"
        pointsOfInterestLabel.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(pointsOfInterestLabel)
        view.addSubview(pointsOfInterestSwitch)
        
        // poi label vertical alignment
        let pointsOfInterestLabelTopConstraint = pointsOfInterestLabel.topAnchor.constraint(equalTo: pointsOfInterestSwitch.topAnchor)
        let pointsOfInterestLabelBottomConstraint = pointsOfInterestLabel.bottomAnchor.constraint(equalTo: pointsOfInterestSwitch.bottomAnchor)
        // poi label horizontal alignment
        let pointsOfInterestLabelLeadingConstraint = pointsOfInterestLabel.leadingAnchor.constraint(equalTo: margins.leadingAnchor)
        
        pointsOfInterestLabelLeadingConstraint.isActive = true
        pointsOfInterestLabelTopConstraint.isActive = true
        pointsOfInterestLabelBottomConstraint.isActive = true
        
        // poi switch alignments
        let pointsOfInterestSwitchTopCoinstraint = pointsOfInterestSwitch.topAnchor.constraint(equalTo: segmentedControl.bottomAnchor, constant: 8)
        let pointsOfinterestSwitchLeadingCoinstraint = pointsOfInterestSwitch.leadingAnchor.constraint(equalTo: pointsOfInterestLabel.trailingAnchor, constant: 8)
        
        pointsOfInterestSwitchTopCoinstraint.isActive = true
        pointsOfinterestSwitchLeadingCoinstraint.isActive = true
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("map loaded")
    }
    
    func mapViewWillStartLoadingMap(_ mapView: MKMapView) {
        print("locating started")
        mapView.showsUserLocation = true
        
    }

    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        print("user locaation did update")
        mapView.setRegion(MKCoordinateRegion.init(center: userLocation.coordinate, latitudinalMeters: 200, longitudinalMeters: 200), animated: true)
    }
    

    @objc func mapTypeChanged(_ segControl: UISegmentedControl) {
        switch segControl.selectedSegmentIndex {
        case 0 :
            mapView.mapType = .standard
        case 1:
            mapView.mapType = .hybrid
        case 2:
            mapView.mapType = .satellite
        default:
            break
        }
    }
    
    @objc func pointsOfInterestSwitchChanged (_ switchControl: UISwitch) {
        if switchControl.isOn {
            mapView.pointOfInterestFilter = MKPointOfInterestFilter.includingAll
        } else {
            mapView.pointOfInterestFilter = MKPointOfInterestFilter.excludingAll
        }
    }
}
