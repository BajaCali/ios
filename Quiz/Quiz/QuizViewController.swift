//
//  ViewController.swift
//  Quiz
//
//  Created by Michal Němec on 15.02.2021.
//

import UIKit

class QuizViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        questionLabel.text = questions[currentQuestionIndex]
        
        print("quiz loaded")
    }

    @IBOutlet var questionLabel: UILabel!
    @IBOutlet var answerLabel: UILabel!
    
    let questions: [String] = [
        "Where is Madagaskar?",
        "How many people are here?",
        "How to sleep?"
    ]
    let answers: [String] = [
        "Few miles from Africa's coast to the east.",
        "More than seven and a half of billion.",
        "Try to close your eyes."
    ]
    var currentQuestionIndex: Int = 0

    @IBAction func showNextQuestion(_ sender: UIButton) {
        currentQuestionIndex += 1
        
        if currentQuestionIndex == questions.count {
            currentQuestionIndex = 0
        }
        
        questionLabel.text = questions[currentQuestionIndex]
        answerLabel.text = "???"
    }
    
    @IBAction func showAnswer(_ sender: UIButton) {
        answerLabel.text = answers[currentQuestionIndex]
    }

}

